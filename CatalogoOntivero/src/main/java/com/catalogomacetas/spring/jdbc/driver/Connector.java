package com.catalogomacetas.spring.jdbc.driver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.springframework.stereotype.Component;

@Component
public class Connector {

	public Connection getConnection() {
		try {
			return DriverManager.getConnection("jdbc:mysql://localhost:3306/listado_macetas", "root", "");
		} catch (SQLException e) {
			System.out.println("Error en la conexión con la B/D");
			e.printStackTrace();
		}
		return null;
	}
}
