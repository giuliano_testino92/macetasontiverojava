package com.catalogomacetas.spring.jdbc.driver;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

//@PropertySource("classpath:database.properties")
@Component
public class ConnectionManager {

	/*
	 * @Value("${db.url}") private String url;
	 * 
	 * @Value("${db.user}") private String user;
	 * 
	 * @Value("${db.password}") private String password;
	 */
	@Autowired
	private Connector conn;

	public ResultSet execute(String query) {

		try {
			Statement myStatement = conn.getConnection().createStatement();
			return myStatement.executeQuery(query);
		} catch (Exception e) {
			System.out.println("Error en la conexión con la B/D");
			e.printStackTrace();
		}
		return null;
	}

	public Connector getConnector() {
		return conn;
	}
	
	public String getElementByColumnName(ResultSet rs, String key) {
		String result = "";
		try {
			result = rs.getString(key);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
}
