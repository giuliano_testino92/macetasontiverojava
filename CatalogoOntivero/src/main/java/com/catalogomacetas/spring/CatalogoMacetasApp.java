package com.catalogomacetas.spring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatalogoMacetasApp {

	public static void main(String[] args) {
		SpringApplication.run(CatalogoMacetasApp.class, args);
	}

}
