package com.catalogomacetas.spring.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class InputController {

	@RequestMapping(value = "showForm")
	public String showForm() {
		return "formulario_macetas";
	}
	
	@RequestMapping(value="inputForm", method = RequestMethod.GET)
	public String returnDouble(HttpServletRequest request, Model model) {
		model.addAttribute("double",2*(Integer.parseInt(request.getParameter("input"))));
		return "resultado_formulario";
	}
}
