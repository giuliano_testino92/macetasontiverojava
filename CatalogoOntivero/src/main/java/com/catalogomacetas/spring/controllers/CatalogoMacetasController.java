package com.catalogomacetas.spring.controllers;

import static com.catalogomacetas.email.EmailUtils.sendEmail;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.catalogomacetas.adapters.AdapterMaceta;
import com.catalogomacetas.adapters.AdapterMacetasByFormato;
import com.catalogomacetas.adapters.AdapterPedido;
import com.catalogomacetas.json.JsonGenerator;
import com.catalogomacetas.models.Maceta;
import com.catalogomacetas.models.Pedido;
import com.catalogomacetas.spring.services.IMacetasService;

@Controller
public class CatalogoMacetasController {

	// @Autowired
	// private MacetasService macetasService;

	@Autowired
	private IMacetasService macetasService;

	@RequestMapping(value = { "/", "" }, method = RequestMethod.GET)
	public String showPage(Model model) {
		model.addAttribute("data", JsonGenerator
				.convertObjectToJSON(AdapterMacetasByFormato.getAdapters(macetasService.getListadosByFormatos())));
		return "listado_macetas";
	}

	@RequestMapping(value = { "/solicitar" }, method = RequestMethod.POST)
	public String goSolicitar(HttpServletRequest request, @RequestParam String jsonStrPedido, Model model) {

		JSONObject objPedido = JsonGenerator.convertStringToObject(jsonStrPedido);

		List<Maceta> listaPedido = getListaFromJson(objPedido);

		model.addAttribute("data", JsonGenerator.convertObjectToJSON(new AdapterPedido(
				Pedido.builder().total((Long) objPedido.get("total")).listadoMacetas(listaPedido).build())));

		return "detalle_pedido";
	}

	@RequestMapping(value = { "/pedidoMail" }, method = RequestMethod.POST)
	public String sendMail(HttpServletRequest request, @RequestParam String nombre, @RequestParam String mail,
			@RequestParam String msjMail, @RequestParam(required = false) String celular, @RequestParam String pedido,
			Model model) {

		String telStr = (celular != null) ? ("<br>&emsp; Tel.: " + celular) : "";

		String mailMsjBody = msjMail + "<br><br>" + pedido + "<br><br>" + "Info: <br>" + " &emsp;Nombre: " + nombre
				+ " <br> &emsp;Mail: " + mail + telStr;

		String emailID = "gtestino1992@gmail.com";

		Properties props = System.getProperties();

		props.put("mail.smtp.host", "smtp.gmail.com");
		//props.put("mail.smtp.socketFactory.port", "465");
		//props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.port", "465");

		// props.put("mail.smtp.port", "465"); // 465
		props.put("mail.smtp.auth", "true");
		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			@Override
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(emailID, "federer92");
			}
		});

		sendEmail(session, emailID, "Pedido de Macetas", mailMsjBody);

		return "envio_pedido_ok";
	}

	private List<Maceta> getListaFromJson(JSONObject obj) {
		List<Maceta> listaPedido = new ArrayList<>();
		Long cantModelo = (Long) obj.get("cantModelos");
		for (int i = 0; i < cantModelo; i++) {
			JSONArray macetasList = (JSONArray) obj.get("macetas");
			JSONObject macetaJson = (JSONObject) macetasList.get(i);
			Maceta maceta = macetasService.getMacetaByCodigoNew((String) macetaJson.get("cod_new"));
			maceta.setCantSolicitada(Integer.parseInt((String) macetaJson.get("cant")));
			listaPedido.add(maceta);
		}

		return listaPedido;
	}

}
