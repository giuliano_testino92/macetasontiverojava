package com.catalogomacetas.spring.mappers;

import java.util.List;

import com.catalogomacetas.models.FormatoMaceta;
import com.catalogomacetas.models.Maceta;

public interface IMacetasMapper {

	public List<Maceta> getListado();
	
	public List<FormatoMaceta> getFormatos();
	
	public List<Maceta> getListaByFormato(FormatoMaceta formato);
	
	public Maceta getMacetaByCodigoNew(String codigoNew);
}
