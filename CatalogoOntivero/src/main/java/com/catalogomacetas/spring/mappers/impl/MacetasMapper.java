package com.catalogomacetas.spring.mappers.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.catalogomacetas.models.FormatoMaceta;
import com.catalogomacetas.models.Maceta;
import com.catalogomacetas.spring.jdbc.driver.ConnectionManager;
import com.catalogomacetas.spring.mappers.IMacetasMapper;

@Component
public class MacetasMapper implements IMacetasMapper {

	@Autowired
	private ConnectionManager connManager;

	public List<Maceta> getListado() {
		List<Maceta> listaMacetas = new ArrayList<>();
		ResultSet rs = connManager.execute("SELECT * FROM lista_macetas");
		try {
			while (rs.next()) {
				String codigo = rs.getString("CODIGO");
				BigDecimal precio = new BigDecimal(rs.getString("PRECIO"));
				BigDecimal largo = new BigDecimal(rs.getString("LARGO"));
				BigDecimal ancho = new BigDecimal(rs.getString("ANCHO"));
				BigDecimal alto = new BigDecimal(rs.getString("ALTO"));
				Integer capacidad = Integer.parseInt(rs.getString("CAPACIDAD"));
				Integer stock = Integer.parseInt(rs.getString("STOCK"));
				String codigoNew = rs.getString("codigo_nuevo");
				List<String> fotos = Arrays.asList(rs.getString("FOTOS").split("\\|"));
				FormatoMaceta formato = FormatoMaceta.getFormatoByCode(rs.getString("FORMATO"));
				listaMacetas.add(Maceta.builder().codigo(codigo).codigoNew(codigoNew).precio(precio)
						.capacidad(capacidad).largo(largo).alto(alto).ancho(ancho).fotos(fotos).formato(formato)
						.stock(stock).build());
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error al ejecutar el query", e);
		}
		return listaMacetas;
	}

	@Override
	public List<FormatoMaceta> getFormatos() {
		List<FormatoMaceta> formatos = new ArrayList<>();
		ResultSet rs = connManager.execute("SELECT DISTINCT FORMATO FROM lista_macetas");
		try {
			while (rs.next()) {
				formatos.add(FormatoMaceta.getFormatoByCode(rs.getString("FORMATO")));
			}
		} catch (SQLException e) {
			throw new RuntimeException("Error al ejecutar el query", e);
		}
		return formatos;
	}

	@Override
	public List<Maceta> getListaByFormato(FormatoMaceta formato) {
		List<Maceta> listaMacetas = new ArrayList<>();
		Connection conn = connManager.getConnector().getConnection();
		PreparedStatement stm;
		try {
			stm = conn.prepareStatement("SELECT * FROM lista_macetas WHERE FORMATO = ?");
			stm.setString(1, formato.getCode());
			ResultSet rs = stm.executeQuery();

			while (rs.next()) {
				String codigo = rs.getString("CODIGO");
				BigDecimal precio = new BigDecimal(rs.getString("PRECIO"));
				BigDecimal largo = new BigDecimal(rs.getString("LARGO"));
				BigDecimal ancho = new BigDecimal(rs.getString("ANCHO"));
				BigDecimal alto = new BigDecimal(rs.getString("ALTO"));
				Integer capacidad = Integer.parseInt(rs.getString("CAPACIDAD"));
				Integer stock = Integer.parseInt(rs.getString("STOCK"));
				String codigoNew = rs.getString("codigo_nuevo");
				List<String> fotos = Arrays.asList(rs.getString("FOTOS").split("\\|"));
				FormatoMaceta formatoMaceta = FormatoMaceta.getFormatoByCode(rs.getString("FORMATO"));
				listaMacetas.add(Maceta.builder().codigo(codigo).codigoNew(codigoNew).precio(precio)
						.capacidad(capacidad).largo(largo).alto(alto).ancho(ancho).fotos(fotos).formato(formatoMaceta)
						.stock(stock).build());
			}

		} catch (SQLException e) {
			throw new RuntimeException("Error al ejecutar el query", e);
		}
		return listaMacetas;
	}

	@Override
	public Maceta getMacetaByCodigoNew(String codigoNew) {
		Maceta maceta;
		Connection conn = connManager.getConnector().getConnection();
		PreparedStatement stm;
		try {
			stm = conn.prepareStatement("SELECT * FROM lista_macetas WHERE CODIGO_NUEVO = ?");
			stm.setString(1, codigoNew);
			ResultSet rs = stm.executeQuery();
			rs.next();
			String codigo = rs.getString("CODIGO");
			BigDecimal precio = new BigDecimal(rs.getString("PRECIO"));
			BigDecimal largo = new BigDecimal(rs.getString("LARGO"));
			BigDecimal ancho = new BigDecimal(rs.getString("ANCHO"));
			BigDecimal alto = new BigDecimal(rs.getString("ALTO"));
			Integer capacidad = Integer.parseInt(rs.getString("CAPACIDAD"));
			Integer stock = Integer.parseInt(rs.getString("STOCK"));
			List<String> fotos = Arrays.asList(rs.getString("FOTOS").split("\\|"));
			FormatoMaceta formatoMaceta = FormatoMaceta.getFormatoByCode(rs.getString("FORMATO"));
			maceta = Maceta.builder().codigo(codigo).codigoNew(codigoNew).precio(precio)
					.capacidad(capacidad).largo(largo).alto(alto).ancho(ancho).fotos(fotos).formato(formatoMaceta)
					.stock(stock).build();
			

		} catch (SQLException e) {
			throw new RuntimeException("Error al ejecutar el query", e);
		}
		
		return maceta;
	}

}
