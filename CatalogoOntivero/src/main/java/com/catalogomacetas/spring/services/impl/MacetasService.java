package com.catalogomacetas.spring.services.impl;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.catalogomacetas.models.FormatoMaceta;
import com.catalogomacetas.models.Maceta;
import com.catalogomacetas.spring.mappers.IMacetasMapper;
import com.catalogomacetas.spring.services.IMacetasService;

@Component
public class MacetasService implements IMacetasService {

	@Autowired
	private IMacetasMapper macetasMapper;

	@Override
	public List<Maceta> getListado() {
		return macetasMapper.getListado();
	}

	@Override
	public HashMap<String, List<Maceta>> getListadosByFormatos() {
		HashMap<String, List<Maceta>> map = new HashMap<>();
		List<FormatoMaceta> formatos = macetasMapper.getFormatos();
		for (FormatoMaceta formato : formatos) {
			map.put(formato.getCode(), macetasMapper.getListaByFormato(formato));
		}
		return map;
	}

	@Override
	public Maceta getMacetaByCodigoNew(String codigoNew) {
		return macetasMapper.getMacetaByCodigoNew(codigoNew);
	}

}
