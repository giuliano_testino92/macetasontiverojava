package com.catalogomacetas.spring.services.mock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.catalogomacetas.models.FormatoMaceta;
import com.catalogomacetas.models.Maceta;
import com.catalogomacetas.spring.services.IMacetasService;

//@Component
public class MacetasServiceMock implements IMacetasService {

	@Override
	public List<Maceta> getListado() {
		List<Maceta> listaMacetas = new ArrayList<>();
		listaMacetas.add(Maceta.builder().codigo("OA24").codigoNew("001").precio(new BigDecimal(50))
				.formato(FormatoMaceta.OVAL).alto(new BigDecimal(2)).largo(new BigDecimal(5)).ancho(new BigDecimal(4))
				.fotos(Arrays.asList(new String[] { "001" })).build());
		listaMacetas.add(Maceta.builder().codigo("RR27").codigoNew("002").precio(new BigDecimal(100))
				.formato(FormatoMaceta.RECTANGULAR).alto(new BigDecimal(3)).largo(new BigDecimal(10))
				.ancho(new BigDecimal(4)).fotos(Arrays.asList(new String[] { "002" })).build());
		return listaMacetas;
	}

	@Override
	public HashMap<String, List<Maceta>> getListadosByFormatos() {
		return null;
	}

	@Override
	public Maceta getMacetaByCodigoNew(String codigoNew) {
		return null;
	}

}
