package com.catalogomacetas.spring.services;

import java.util.HashMap;
import java.util.List;

import com.catalogomacetas.models.Maceta;

public interface IMacetasService {

	public List<Maceta> getListado();

	public HashMap<String, List<Maceta>> getListadosByFormatos();
	
	public Maceta getMacetaByCodigoNew(String codigoNew);
}
