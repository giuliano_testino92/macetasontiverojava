package com.catalogomacetas.models;

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

public enum FormatoMaceta {
	
	OVAL("OVA"), 
	RECTANGULAR("REC"), 
	CASCADA("CAS"), 
	BOSQUE("BSQ"),
	REDONDA("RED"), 
	OCTOGONAL("OCT"), 
	CUADRADA("SQR"), 
	HEXAGONAL("HEX"),
	OTRAS("OTR"), 
	LAGOS("LAG"),
	GRES("GRS");
	
	private String code;
	private static final Map<String,FormatoMaceta> lookup = new HashMap<String,FormatoMaceta>();
    
	static {
        for(FormatoMaceta s : EnumSet.allOf(FormatoMaceta.class))
            lookup.put(s.getCode(), s);
    }
	 
    FormatoMaceta(String code) {
        this.code = code;
    }
 
    public String getCode() {
        return code;
    } 
    
    public static FormatoMaceta getFormatoByCode(String code) { 
        return lookup.get(code);    
    }
    
}
