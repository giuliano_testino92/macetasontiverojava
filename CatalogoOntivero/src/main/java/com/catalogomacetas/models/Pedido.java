package com.catalogomacetas.models;

import java.util.List;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Pedido {

	private List<Maceta> listadoMacetas;
	private Long total;

}
