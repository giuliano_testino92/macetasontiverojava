package com.catalogomacetas.adapters;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.catalogomacetas.models.Maceta;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AdapterMacetasByFormato {

	@JsonIgnore
	private String formato;

	@JsonIgnore
	private List<Maceta> lista;

	public AdapterMacetasByFormato(String formato, List<Maceta> lista) {
		this.formato = formato;
		this.lista = lista;
	}

	public static List<AdapterMacetasByFormato> getAdapters(HashMap<String, List<Maceta>> listados) {
		List<AdapterMacetasByFormato> adapters = new ArrayList<>();
		for (String key : listados.keySet()) {
			adapters.add(new AdapterMacetasByFormato(key, listados.get(key)));
		}
		return adapters;
	}

	@JsonProperty
	public String getFormato() {
		return formato;
	}

	@JsonProperty
	public List<AdapterMaceta> getMacetas() {
		return AdapterMaceta.getAdapters(lista);
	}

}
