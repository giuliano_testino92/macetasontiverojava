package com.catalogomacetas.adapters;

import java.util.List;

import com.catalogomacetas.models.Pedido;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AdapterPedido {

	@JsonIgnore
	private Pedido pedido;
	
	public AdapterPedido(Pedido pedido) {
		this.pedido = pedido;
	}
	
	@JsonProperty
	public List<AdapterMaceta> getListadoMacetas(){
		return AdapterMaceta.getAdapters(pedido.getListadoMacetas());
	}
	
	@JsonProperty
	public Long getTotal() {
		return pedido.getTotal();
	}
}
