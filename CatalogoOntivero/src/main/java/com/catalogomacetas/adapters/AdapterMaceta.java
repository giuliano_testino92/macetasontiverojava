package com.catalogomacetas.adapters;

import java.util.ArrayList;
import java.util.List;

import com.catalogomacetas.models.Maceta;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class AdapterMaceta {

	@JsonIgnore
	private Maceta maceta;

	public AdapterMaceta(Maceta maceta) {
		this.maceta = maceta;
	}

	public static List<AdapterMaceta> getAdapters(List<Maceta> listaMacetas) {
		List<AdapterMaceta> adapters = new ArrayList<>();
		for (Maceta maceta : listaMacetas) {
			adapters.add(new AdapterMaceta(maceta));
		}
		return adapters;
	}

	@JsonProperty
	public String getCodigo() {
		return maceta.getCodigo();
	}
	
	@JsonProperty
	public String getAlto() {
		return maceta.getAlto().toString();
	}
	
	@JsonProperty
	public String getAncho() {
		return maceta.getAncho().toString();
	}
	
	@JsonProperty
	public String getLargo() {
		return maceta.getLargo().toString();
	}
	
	@JsonProperty
	public Integer getCantSolicitada() {
		return maceta.getCantSolicitada() != null ? maceta.getCantSolicitada()  : 0;
	}
	
	@JsonProperty
	public String getCapacidad() {
		return maceta.getCapacidad()!= null ? maceta.getCapacidad().toString() : "";
	}
	
	@JsonProperty
	public String getCodigoNew() {
		return maceta.getCodigoNew();
	}
	
	@JsonProperty
	public String getFormato() {
		return maceta.getFormato().getCode();
	}
	
	@JsonProperty
	public List<String> getFotos() {
		return maceta.getFotos();
	}
	
	@JsonProperty
	public String getPrecio() {
		return maceta.getPrecio().toString();
	}
	
	@JsonProperty
	public Integer getStock() {
		return maceta.getStock() != null ? maceta.getStock() : 0;
	}
}
