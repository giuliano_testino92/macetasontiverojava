function parseDetallePedido(){
	var listaPedido = global.dataPedido.listadoMacetas;
	var totalCompra = global.dataPedido.total;
    for (var i=0;i<listaPedido.length;i++){
        var html = "<tr> <td> " + listaPedido[i].codigo + "</td><td>$";
        html += listaPedido[i].precio + "</td><td>" + listaPedido[i].largo;
        html += "</td><td>" +  listaPedido[i].ancho  + "</td><td>" 
            +  listaPedido[i].alto +  "</td><td>" ;
        html +=  listaPedido[i].cantSolicitada  + "</td><td> $"
            + listaPedido[i].precio*listaPedido[i].cantSolicitada + "</td></tr>";
        $('#tabla-detalle-pedido').append(html);
    }

    $('#total').html("<strong>Total: $"+totalCompra+"</strong>");

    $('#send-mail-button').click(parseInfoMail);
}

function parseInfoMail(){

    var nombrePedido = $('#nombre').val();
    var mailPedido = $('#mail').val();
    var msjPedido = $('#msj-mail').val();
    var celular = $('#celular').val();
    
    if(!validateInfo(nombrePedido,mailPedido,msjPedido))
        return;
    else
        $.redirectPost("/pedidoMail", {"nombre" : nombrePedido, "mail" : mailPedido,
             "msjMail" : msjPedido , "celular" : celular, "pedido" : textoPedido(global.dataPedido.listadoMacetas,global.dataPedido.total)});    
    
}

function textoPedido(pedido, total){
    var str = "<br><br>Pedido: <br><br>";
    for(var i=0;i<pedido.length;i++){
        str += "&emsp;" + pedido[i].codigo + "&emsp;" + pedido[i].cantSolicitada + "<br>";
    }
    str += "<br>&emsp;Total:  $" + total;
    return str;
}
$.extend(
    {
        redirectPost: function(location, args)
        {
            var form = $('<form></form>');
            form.attr("method", "post");
            form.attr("action", location);
    
            $.each( args, function( key, value ) {
                var field = $('<input></input>');
    
                field.attr("type", "hidden");
                field.attr("name", key);
                field.attr("value", value);
    
                form.append(field);
            });
            $(form).appendTo('body').submit();
        }
    });


function validateInfo(nombrePedido,mailPedido,msjPedido){
    var isValid = true;
    if(nombrePedido==null || nombrePedido==""){
        $('#nombre').addClass('error-highlight');
        isValid = false;
    } else
        $('#nombre').removeClass('error-highlight');
    if(mailPedido==null || mailPedido==""){
        $('#mail').addClass('error-highlight');
        isValid = false;
    } else 
        $('#mail').removeClass('error-highlight');
    if(msjPedido==null || msjPedido==""){
        $('#msj-mail').addClass('error-highlight');
        isValid = false;
    } else 
        $('#msj-mail').removeClass('error-highlight');
    var mailCheckStr1 = mailPedido.split("@")[0];
    var mailCheckStr2 = mailPedido.split("@")[1];
    if(mailCheckStr1==null || mailCheckStr1=="" || mailCheckStr2==null || mailCheckStr2==""){
        $('#mail').addClass('error-highlight');
        isValid = false;
    } else 
        $('#mail').removeClass('error-highlight');
    
    return isValid;
}
