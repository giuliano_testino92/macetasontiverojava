$(document).ready(function(){
        $("[data-toggle='under-list']").on('click', function() {
            var cod_new = $(this).attr("id");
            var format_new = $(this).attr('format');
            var objNew = $('.foto-' + format_new);
            var cod_old = objNew.attr('img-actual');
            var format_old="";
            if(cod_old!="")
                format_old = $('#'+cod_old).attr('format');
            if($(this).attr('toggle')=='off'){
                
                if (format_old==format_new){
                    $('#'+cod_old).attr('toggle','off');
                } 
                $(this).attr('toggle','on');
                
                var cod = $(document).find('#cod'+cod_new).html();  
                var output = "<p  ><img id='foto-maceta-" + format_new + "' class='foto-maceta borderimg' src='" + $(this).data('img') + "'/>";
                output+= "</p><p style='text-align:center;'>";
                output+= "<label class='label-modelo'>Modelo: " + cod + "</label></p>";
                output+= "<div id='myModal' class='modal modal-" + format_new + "'> <span class='close close-" + format_new + "'>&times;</span>";
                output+= "<img class='modal-content' id='img-maceta-full-" + format_new + "'><label class='label-modelo caption'></label></div>";
                objNew.html(output);
                objNew.attr('img-actual',cod_new);
                objNew.slideDown();
                
                var img = $("#foto-maceta-"+format_new);
                var span =$(".close-"+format_new)[0];
                var modal = $(".modal-"+format_new);
                var modalImg = $("#img-maceta-full-"+format_new);
                var captionText = $(".caption");
                
                img.click(function(){
                    modal.css("display", "block");
                    modalImg.attr('src', this.src);
                    captionText.html(cod);
                  });

                span.onclick = function() { 
                    modal.css("display", "none");
                }
                
                                   
            } else {
                
                $(this).attr('toggle','off');
                objNew.hide(); 
            }
        });
});

window.onscroll = function() {scrollCarrito()};



function scrollCarrito() {
    var carrito = $("#carrito")[0];
    var offsetCarrito = 514;

    if($('#open-filtro-formato-label').attr('toggle')=='on'){
        offsetCarrito = 796;
    } else if($('#open-filtro-tamano-label').attr('toggle')=='on'){
        offsetCarrito = 1223;
    }
    if (window.pageYOffset> offsetCarrito) {
        $(carrito).addClass("sticky");
        $('#carrito-card').css("left", "-105px");
        $('#carrito-card').css("width", "350px");
    } else {
        $(carrito).removeClass("sticky");
        $('#carrito-card').css("left", "-105px");
    }
}
