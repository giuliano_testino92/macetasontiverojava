<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>

<%@include file="header.jsp" %>
      
  
</head>


<body style='background-color: darkseagreen;'>

<br><br><br>
<h1 style='text-decoration:underline;text-align:center'>Catálogo Macetas Bonsai Ontivero</h1>

<br><br>
<script src='js/fotos-popover.js'></script>
<script src='js/filtros.js'></script>
<script src='js/carrito.js'></script>
<script src='js/tablas-header.js'></script>
<script src='js/tablas-body.js'></script>



<div class='container'>
  <div class='row'>
    <div class='col-sm-12'>
      
	  <p><h2>Instrucciones</h2></p>
      
      <ul>
        <li class='text-explain'>
          Presione  &nbsp<a class='btn btn-primary btn-explain' style='cursor:auto;' title='Ver foto'><i class='fa fa-camera fa-lg'></i></a>  &nbsppara ver la imagen debajo de cada listado.
        </li>
        <li class='text-explain'>
          Presione  &nbsp<a class='btn btn-primary btn-explain' style='cursor:auto;' title='Agregar al carrito'><i class='fa fa-plus fa-lg'></i></a>  &nbsp para ver agregar el modelo al &nbsp
            <button class='btn btn-secondary' style='background-color:green;width:115px;cursor:auto;'>
              <i class='fa fa-shopping-cart fa-lg'></i>&nbsp&nbsp&nbsp<strong>Carrito</strong>&nbsp
            </button>
        </li>
        <li class='text-explain'>
          Utilice los  &nbsp<button class='btn btn-primary' style='cursor:auto;'><strong>Filtros</strong></button>  &nbsp para filtrar por formato y tamaño.
        </li>
      </ul>
    </div>
  </div>
</div>
<br><br>
<div class='container'>
  <div class='row'>
    <div class='col-sm-8 col-listados'>
   
    </div>
    <div class='col-sm-4'>
      <div class='container'>
        <div class='filtros' style='position:relative;right:-200px;'>
         
            <div class='row'>
              <%@include file="filtro_formatos.jsp" %>
      
            </div>
            <br><br>
            <div class='row'>
               <%@include file="filtro_tamano.jsp" %>
            </div>
            <br><br>
          
        </div>
       

        <div class='carrito' id='carrito' >
          <div style='position:relative;left:200px;'>
            
              
               <%@include file="carrito.jsp" %>
     
            
          </div>
        </div>
      </div>

      
      
    </div>
  </div>
</div>
<br><br><br><br>  

<script type="text/javascript">
	
	window.fotoPopOver = "";
	window.cantCarrito = 0;
	window.maxCantPageCarrito = 5;
	window.pageActualCarrito = 1;
  
	var global = {};
	global.data = ${data};
	console.log(global.data);
	makeListados();
</script>

</body>

</html>


