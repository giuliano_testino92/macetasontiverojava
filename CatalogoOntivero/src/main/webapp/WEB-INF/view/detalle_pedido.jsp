<!DOCTYPE html>
<html>

<!--  ?php
if (!isset($_SERVER['HTTP_REFERER'])) { 
    die("No puedes acceder a la p�gina directamente");
    }
?-->
<head>

	<%@include file="header.jsp" %>
      
</head>

<script src='/js/solicitar-pedido.js'></script>


<br><br><br>
<h1 style='text-decoration:underline;text-align:center'>Detalle</h1>
<br>

<body style='background-color: darkseagreen;'>

<div class='container'>
    <div class='row'>
        
        <div class='col'>
        	<br>
			<%@include file="detalle_pedido_tabla.jsp" %>
        </div>
        
    </div>
    <br><br>

    <div class='container'>
        <div class='row'>
            
            <div class='col-sm-12'>
                <ul>
                    <li class='text-explain'>
                        Dejanos un mensaje, tu mail, un tel�fono (opcional) y tu nombre junto con el listado. Te contactaremos en breve.
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <br><br>
    <div class='row'>
        <div class='col'>
            <form id='mail-form'>
                
                <div class='row'>
                    <div class='col-sm-4' style='text-align:center;'>
                        <label > Tu nombre:   </label>
                    </div>
                    <div class='col-sm-4'>
                        <input class='form-control' id='nombre' name='nombre' style='width:500px;' maxlength=30>
                    </div>
                    <div class='col-sm-4'></div>
                </div>
                <br>
                <div class='row'>
                    <div class='col-sm-4' style='text-align:center;'>
                        <label > Tu mail:   </label>
                    </div>
                    <div class='col-sm-4'>
                        <input class='form-control' id='mail' name='mail' style='width:500px;' maxlength=30>
                    </div>
                    <div class='col-sm-4'></div>
                </div>
                
                <br>
                <div class='row'>
                    <div class='col-sm-4' style='text-align:center;'>
                        <label> Celular (opcional):   </label>  
                    </div>
                    <div class='col-sm-4'>
                        <input class='form-control' id='celular' name='celular' style='width:500px;' maxlength=15>
                    </div>
                    <div class='col-sm-4'></div>
                </div>

                <br>
                <div class='row'>
                    <div class='col-sm-4' style='text-align:center;'>
                        <label> Tu mensaje:   </label>  
                    </div>
                    <div class='col-sm-4'>
                        <textarea class='form-control' id='msj-mail' name='msj-mail' style='width:500px;height:200px;'  maxlength=1000></textarea> 
                    </div>
                    <div class='col-sm-4'></div>
                </div>

                

                <br>
                <div class='row'>
                    <div class='col-sm-4'></div>
                    <div class='col-sm-4' style='text-align:center;'>
                        <input type='button' id='send-mail-button' class='btn btn-secondary' value='Enviar mail'>
                    </div>
                    <div class='col-sm-4'></div>
                </div>
            </form>
        </div>
    
    </div>
</div>


<script type="text/javascript">
	
	var global = {}
	global.dataPedido = ${data};
	console.log(global.dataPedido);

	parseDetallePedido();
	
</script>


</body>
</html>
